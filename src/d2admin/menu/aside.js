// 菜单 侧边栏
export default [
  {
    path: '/index',
    title: '目的地管理',
    icon: 'home',
    children: [
      { path: '/page1', title: '目的地列表' },
      { path: '/page2', title: '增加目的地' }
    ]
  },
  {
    path: '/index',
    title: '酒店管理',
    icon: 'home',
    children: [
      { path: '/page1', title: '酒店列表' },
      { path: '/page2', title: '增加酒店' },
      { path: '/page2', title: '套餐管理' }
    ]
  },
  {
    path: '/index',
    title: '订单管理',
    icon: 'home',
    children: [
      { path: '/page1', title: '订单列表' }
    ]
  }
]
